// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATopDownGameCharacter::ATopDownGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("/Game/TopDownGame/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownGameCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	if (Sprinting)
	{
		GetForwardDotProduct() > Angle ? Sprinting = true : Sprinting = false, Walking = true;
		ChangeMovementState();
	}
	MovementTick(DeltaSeconds);
}

void ATopDownGameCharacter::SetupPlayerInputComponent(UInputComponent* InputComponents)
{
	Super::SetupPlayerInputComponent(InputComponents);

	InputComponents->BindAxis(TEXT("MoveForward"), this, &ATopDownGameCharacter::InputAxisX);
	InputComponents->BindAxis(TEXT("MoveRight"), this, &ATopDownGameCharacter::InputAxisY);
}

void ATopDownGameCharacter::InputAxisX(float Value)
{
	AxisX = Value;

}

void ATopDownGameCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownGameCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		FRotator LookHere = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
		SetActorRotation(FQuat(FRotator(0.f, LookHere.Yaw, 0.f)));
	}
}

void ATopDownGameCharacter::CharacterUpdate()
{
	float ResSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	case EMovementState::Slowed_State:
		ResSpeed = MovementInfo.SlowedSpeed;
		break;
	default:
		break;
	}
	UE_LOG(LogTemp, Warning, TEXT("CharacterMoveState: %f"), ResSpeed);
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownGameCharacter::ChangeMovementState()
{
		if (!Sprinting && !Aiming)
		{
			MovementState = EMovementState::Walk_State;
		}
		else
		{
			if (Sprinting)
			{
				Walking = false;
				Aiming = false;
				MovementState = EMovementState::Sprint_State;
			}
			if (Walking && !Sprinting && Aiming)
			{
				MovementState = EMovementState::Aim_State;
			}
			else
			{
				if (Walking && !Sprinting && !Aiming)
				{
					MovementState = EMovementState::Walk_State;
				}
				else
				{
					if (!Walking && !Sprinting && Aiming)
					{
						MovementState = EMovementState::Aim_State;
					}
				}
			}
		}
	CharacterUpdate();
}

void ATopDownGameCharacter::CameraSlider(float Value)
{
	if (Value != 0)
	{
		if (Value < 0)
		{
			float CameraPositionPlus = CameraBoom->TargetArmLength + CameraHeightChangeDistance;
			if (CameraHeightMax >= CameraPositionPlus)
			{
				CameraBoom->TargetArmLength = CameraPositionPlus;
			}
		}
		else
		{
			float CameraPositionMinus = CameraBoom->TargetArmLength - CameraHeightChangeDistance;
			if (CameraHeightMin <= CameraPositionMinus)
			{
				CameraBoom->TargetArmLength = CameraPositionMinus;
			}
		}
		//UE_LOG(LogTemp, Warning, TEXT("CharacterCameraRange: %f"), CameraBoom->TargetArmLength);
	}
}

float ATopDownGameCharacter::GetForwardDotProduct()
{
	//FVector LastInputVector = GetCharacterMovement()->GetLastInputVector();
	float DProduct = FVector::DotProduct(GetActorForwardVector(), LastControlInputVector);
	return DProduct;
}
