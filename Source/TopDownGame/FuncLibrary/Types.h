// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Sprint_State UMETA(DisplayName = "Sprint State"),
	Slowed_State UMETA(DisplayName = "Slowed State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 400.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 900.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SlowedSpeed = 300.f;
};

UCLASS()
class TOPDOWNGAME_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};