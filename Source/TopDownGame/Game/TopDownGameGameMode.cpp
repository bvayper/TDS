// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownGameGameMode.h"
#include "TopDownGamePlayerController.h"
#include "TopDownGame/Character/TopDownGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownGameGameMode::ATopDownGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownGamePlayerController::StaticClass();
	//// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownGame/Blueprints/Character"));
	//if (PlayerPawnBPClass.Class != nullptr)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
}